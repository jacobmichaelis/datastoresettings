package com.jmichaelis.datastoresettings

import android.content.Context
import androidx.datastore.preferences.core.*
import androidx.datastore.preferences.preferencesDataStore
import androidx.preference.PreferenceDataStore
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class DSDataStore(private val context: Context): PreferenceDataStore() {

    private val Context.dataStore by preferencesDataStore(name = DS_NAME)

    override fun putString(key: String?, value: String?) {
        CoroutineScope(Dispatchers.IO).launch {
            key?.let { k ->
                value?.let { v ->
                    context.dataStore.edit { it[stringPreferencesKey(k)] = v }
                }
            }
        }
    }

    override fun getString(key: String?, defValue: String?): String? {
        return runBlocking {
            key?.let { k ->
                context.dataStore.data.map { it[stringPreferencesKey(k)] }.first()
            } ?: defValue
        }
    }

    override fun putBoolean(key: String?, value: Boolean) {
        CoroutineScope(Dispatchers.IO).launch {
            key?.let { k ->
                context.dataStore.edit { it[booleanPreferencesKey(k)] = value }
            }
        }
    }

    override fun getBoolean(key: String?, defValue: Boolean): Boolean {
        return runBlocking {
            key?.let { k ->
                context.dataStore.data.map { it[booleanPreferencesKey(k)] }.first()
            } ?: defValue
        }
    }

    override fun putStringSet(key: String?, values: MutableSet<String>?) {
        CoroutineScope(Dispatchers.IO).launch {
            key?.let { k ->
                values?.let { v ->
                    context.dataStore.edit { it[stringSetPreferencesKey(k)] = v }
                }
            }
        }
    }

    override fun getStringSet(key: String?, defValues: MutableSet<String>?): MutableSet<String> {
        return runBlocking {
            key?.let { k ->
                context.dataStore.data.map { it[stringSetPreferencesKey(k)] }.first()
            } as? MutableSet<String> ?: defValues ?: mutableSetOf()
        }
    }

    override fun putInt(key: String?, value: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            key?.let { k ->
                context.dataStore.edit { it[intPreferencesKey(k)] = value }
            }
        }
    }

    override fun getInt(key: String?, defValue: Int): Int {
        return runBlocking {
            key?.let { k ->
                context.dataStore.data.map { it[intPreferencesKey(k)] }.first()
            } ?: defValue
        }
    }

    override fun putLong(key: String?, value: Long) {
        CoroutineScope(Dispatchers.IO).launch {
            key?.let { k ->
                context.dataStore.edit { it[longPreferencesKey(k)] = value }
            }
        }
    }

    override fun getLong(key: String?, defValue: Long): Long {
        return runBlocking {
            key?.let { k ->
                context.dataStore.data.map { it[longPreferencesKey(k)] }.first()
            } ?: defValue
        }
    }

    override fun putFloat(key: String?, value: Float) {
        CoroutineScope(Dispatchers.IO).launch {
            key?.let { k ->
                context.dataStore.edit { it[floatPreferencesKey(k)] = value }
            }
        }
    }

    override fun getFloat(key: String?, defValue: Float): Float {
        return runBlocking {
            key?.let { k ->
                context.dataStore.data.map { it[floatPreferencesKey(k)] }.first()
            } ?: defValue
        }
    }

    companion object {
        const val DS_NAME = "TEST_DATASTORE"
    }
}