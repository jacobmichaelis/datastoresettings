package com.jmichaelis.datastoresettings

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceFragmentCompat

/**
 * Sample Settings app.
 *
 * The goal of this app is to use the new DataStore library
 * as the PreferenceDataStore instead of the default SharedPreferences
 * because SharedPreferences are no longer recommended in general.
 *
 * Key classes/files to look at:
 * @see DSDataStore
 * @see res/xml/root_preferences.xml
 */

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.settings, SettingsFragment())
                .commit()
        }
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            preferenceManager.preferenceDataStore = DSDataStore(requireContext())
            setPreferencesFromResource(R.xml.root_preferences, rootKey)
        }
    }
}